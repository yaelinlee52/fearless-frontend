window.addEventListener('DOMContentLoaded', async () => {

    const url = 'http://localhost:8000/api/states/';

    try {
        const response = await fetch(url);

        if (!response.ok) {
            throw new Error('Failed to fetch data')
        }

        const data = await response.json();
        const selectTag = document.getElementById('state');

        if (!selectTag) {
            throw new Error("no state element found");
        }

        for (let state of data.states) {
            // Create an 'option' element
            var opt = document.createElement('option');
            opt.value = state.abbreviation;
            opt.innerHTML = state.name;
            selectTag.appendChild(opt);
        }
    } catch (e) {
        console.error('error!!!', e)
    }
  });
